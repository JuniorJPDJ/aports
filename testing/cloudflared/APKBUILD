# Contributor: Piper McCorkle <piper@cloudflare.com>
# Maintainer: Piper McCorkle <piper@cloudflare.com>
pkgname=cloudflared
pkgver=2023.6.1
pkgrel=0
pkgdesc="Cloudflare Tunnel client"
url="https://github.com/cloudflare/cloudflared"
arch="aarch64 x86 x86_64"
license="Apache-2.0"
makedepends="go gettext"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/cloudflare/cloudflared/archive/refs/tags/$pkgver.tar.gz
	goflags.patch
	"
options="!check" # require privileged icmp sockets

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare
	go mod vendor
}

build() {
	make cloudflared
	DATE="2023" VERSION="$pkgver" envsubst < cloudflared_man_template > cloudflared.1
}

check() {
	_goarch=$(go tool dist env | grep GOARCH | sed 's/^GOARCH="//; s/"$//')
	# Go race detector is only supported on amd64, ppc64le, and arm64
	if [ $_goarch = "amd64" ] || [ $_goarch = "ppc64le" ] || [ $_goarch = "arm64" ]; then
		_race=-race
	fi
	go test -mod=vendor -buildmode=default $_race ./...
}

package() {
	install -D -m755 ./cloudflared "$pkgdir"/usr/bin/cloudflared
	install -D -m644 ./cloudflared.1 "$pkgdir"/usr/share/man/man1/cloudflared.1
}

sha512sums="
692cecfbc147bbc7612764adbc7fecc65d6147735ec560dfda46a3c2b5b1196cfbe578ff28709f0e5b78dc68e8590f8379f038064efc60d3a71b410695189418  cloudflared-2023.6.1.tar.gz
deb54b26ac89da74f6d1016deebdd2a15e302d49c1c2b83787b905e70b00250ddf9568d078c4d1d7b53bba9ab1aa182fbcadca2376a7008f07ce4502a4c6f91c  goflags.patch
"
