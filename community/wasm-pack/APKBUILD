# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasm-pack
pkgver=0.12.0
pkgrel=1
pkgdesc="rust to wasm build tool"
url="https://github.com/rustwasm/wasm-pack"
arch="all !ppc64le !s390x !riscv64" # ring
license="Apache-2.0"
depends="cargo rust-wasm"
makedepends="
	bzip2-dev
	cargo-auditable
	curl-dev
	openssl-dev>3
	wasi-sdk
	zstd-dev
	"
source="https://github.com/rustwasm/wasm-pack/archive/refs/tags/v$pkgver/wasm-pack-v$pkgver.tar.gz"
options="net !check" # most tests fail outside of x86_64

export OPENSSL_NO_VENDOR=1

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libzstd.
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		zstd = { rustc-link-lib = ["zstd"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/wasm-pack \
		-t "$pkgdir"/usr/bin
}

sha512sums="
75fe6556aad5938818ed1866ea061aac749e8ed249fa20d5f8249cbacdab6ab9a1062abdc9044995f566dc53c70ab729772449924ca2d9a0beb701aef7f35d49  wasm-pack-v0.12.0.tar.gz
"
