# Contributor: Sam Nystrom <sam@samnystrom.dev>
# Maintainer: Sam Nystrom <sam@samnystrom.dev>
pkgname=lutgen
pkgver=0.6.1
pkgrel=0
pkgdesc="Blazingly fast interpolated LUT generator and applicator for arbitrary and popular color palettes"
url="https://github.com/ozwaldorf/lutgen-rs"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/ozwaldorf/lutgen-rs/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/lutgen-rs-$pkgver"
options="net" # cargo fetch

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 "$builddir"/target/release/lutgen "$pkgdir"/usr/bin/lutgen
}

sha512sums="
0b59d16fe5d4efc59215358f682f30b60b410637ae505979015c966934ee5b530accc85585449a3526a436cf9c85105e89512c7e8dd2f481e429c9731cc31bfb  lutgen-0.6.1.tar.gz
"
